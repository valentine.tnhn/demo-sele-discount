package com.example.demoselediscount;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Locale;

@SpringBootApplication
public class DemoSeleDiscountApplication {

    public static void main(String[] args) {
        String inPut = "testing";
        Float valuesDiscount = 5f;

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        WebDriverWait driverWait = new WebDriverWait(driver,7);
        driver.manage().window().maximize();
        String baseUrl = "https://au-webhook-adc1.onshopbase.com/checkouts/8002e445bfc8485b90091ec41865f8e5?step=contact_information";
        driver.get(baseUrl);

        //test name discount
        driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]")).sendKeys("testing");
        driver.findElement(By.xpath("//button[contains(text(),'Apply')]")).click();
        String codeDiscount = driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/span[2]")).getText();
        System.out.println("Code discount = " + codeDiscount);

        String textOutPut= codeDiscount.toLowerCase();
        System.out.println("Text in put = " + inPut + "\nText Out put : " + codeDiscount);
        System.out.println(inPut.equals(textOutPut));

        //test values discount
        String discount = driver.findElement(By.xpath("//span[contains(text(),'- $5.00')]")).getText();
        System.out.println("Discount values: " + discount);
        String value = "- $"+ valuesDiscount+"0";
        System.out.println("Values discount input: "+value);
        System.out.println("Values discount output: " + discount);
        System.out.println(value.equals(discount));

        driver.close();

    }

}
